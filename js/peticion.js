fetch("http://dummy.restapiexample.com/api/v1/employees")

.then(res => res.json())

.then(function(res) {


        var array = res.data;
        const form = document.getElementById('form');

        const input = form.getElementsByTagName('input')[0];


        class MiBotonExtendido2 extends HTMLButtonElement {
            constructor() {
                super();

                this.addEventListener('click', function(e) {
                    e.preventDefault();
                    // Obtenemos lo que se introduzca en el input
                    let id = input.value;

                    // Obtenemos solo el usuario que coincida con el id
                    var mi_usuario = array.find(
                        user => user.id == id
                    );


                    // Mostramos resultados

                    var tbody = document.querySelector("#agregar");
                    var separador = " ";

                    var th = document.createElement("tr");
                    var td = document.createElement("td");
                    td.textContent = mi_usuario.id;
                    th.append(td);
                    tbody.append(th);

                    var nombre = mi_usuario.employee_name;
                    var nombre1 = nombre.split(separador);


                    var td1 = document.createElement("td");
                    td1.textContent = nombre1[0];
                    th.append(td1);
                    tbody.append(th);

                    var td2 = document.createElement("td");
                    td2.textContent = nombre1[1];
                    th.append(td2);
                    tbody.append(th);


                    var td3 = document.createElement("td");
                    td3.textContent = mi_usuario.employee_salary;
                    th.append(td3);
                    tbody.append(th);


                    var td4 = document.createElement("td");
                    td4.textContent = mi_usuario.employee_age;
                    th.append(td4);
                    tbody.append(th);

                    console.log(mi_usuario);
                });


            }
            static get ceName() {
                return "mi-boton-extendido2";
            }

            get is() {
                return this.getAttribute("is");
            }
            set is(value) {
                this.setAttribute('is', value || this.ceName);
            }
        }

        customElements.define(MiBotonExtendido2.ceName, MiBotonExtendido2, { extends: "button" });



    })
    .catch(function(err) {
        console.error(err);
    });




class MiBotonExtendido extends HTMLButtonElement {
    constructor() {
        super();
        this.addEventListener('click', function(e) {
            var tbody = document.querySelector("#agregar");
            tbody.textContent = "";

        });
    }
    static get ceName() {
        return "mi-boton-extendido";
    }

    get is() {
        return this.getAttribute("is");
    }
    set is(value) {
        this.setAttribute('is', value || this.ceName);
    }
}

customElements.define(MiBotonExtendido.ceName, MiBotonExtendido, { extends: "button" });

const MiBotonExtendido2 = document.createElement('button', { is: MiBotonExtendido.ceName });